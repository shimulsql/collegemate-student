import Alpine from "alpinejs";

Alpine.store('common', {
    darkMode: true,
    menu: false,
    sticky: false,

    toggleDarkMode(){
        this.darkMode = !this.darkMode;

        localStorage.setItem('_dark', this.darkMode);
    },

    menuShow(){
        this.menu = true;
    },

    menuHide(){
        this.menu = false;
    },

    syncTheme(){
        this.darkMode = localStorage.getItem('_dark') == 'true';
    },

    init(){
        this.syncTheme();
    }
});
