import Alpine from "alpinejs";
import * as bootstrap from 'bootstrap';

import './store'
import demoData from "./demoData";

Alpine.data('demoData', demoData);

Alpine.start();