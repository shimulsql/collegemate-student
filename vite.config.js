import path from 'path';
import { ViteEjsPlugin } from "vite-plugin-ejs";
import fs from 'fs';

export default {
  root: path.resolve(__dirname, 'src'),
  build: {
    outDir: '../dist',
    rollupOptions: {
      input: getHtmlEntries()
    }
  },
  server: {
    port: 8080
  },
  plugins: [
    ViteEjsPlugin(),
  ]
}

function getHtmlEntries() {
  const pagesDir = path.resolve(__dirname, "src/");
  const entries = {};

  // Read all files in the directory
  const files = fs.readdirSync(pagesDir);

  // Filter out HTML files
  const htmlFiles = files.filter((file) => file.endsWith(".html"));

  // Create entries for each HTML file
  htmlFiles.forEach((file) => {
    const name = path.basename(file, ".html");
    entries[name] = path.resolve(pagesDir, file);
  });

  return entries;
}